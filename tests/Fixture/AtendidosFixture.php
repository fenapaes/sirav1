<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AtendidosFixture
 *
 */
class AtendidosFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'nome' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'nascimento' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'cpf' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'rg' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'pai' => ['type' => 'string', 'length' => 200, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'mae' => ['type' => 'string', 'length' => 200, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'sexo_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'raca_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'deficiencia_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'sexo_id' => ['type' => 'index', 'columns' => ['sexo_id'], 'length' => []],
            'raca_id' => ['type' => 'index', 'columns' => ['raca_id'], 'length' => []],
            'deficiencia_id' => ['type' => 'index', 'columns' => ['deficiencia_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'atendidos_ibfk_1' => ['type' => 'foreign', 'columns' => ['sexo_id'], 'references' => ['sexos', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'atendidos_ibfk_2' => ['type' => 'foreign', 'columns' => ['deficiencia_id'], 'references' => ['deficiencias', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_alunos_racas' => ['type' => 'foreign', 'columns' => ['raca_id'], 'references' => ['racas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'nome' => 'Lorem ipsum dolor sit amet',
            'nascimento' => '2015-08-19',
            'cpf' => 'Lorem ipsum dolor ',
            'rg' => 'Lorem ipsum dolor ',
            'pai' => 'Lorem ipsum dolor sit amet',
            'mae' => 'Lorem ipsum dolor sit amet',
            'sexo_id' => 1,
            'raca_id' => 1,
            'deficiencia_id' => 1
        ],
    ];
}
