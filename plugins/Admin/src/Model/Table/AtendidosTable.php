<?php
namespace Admin\Model\Table;

use Admin\Model\Entity\Atendido;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Atendidos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Sexos
 * @property \Cake\ORM\Association\BelongsTo $Racas
 * @property \Cake\ORM\Association\BelongsTo $Deficiencias
 * @property \Cake\ORM\Association\HasMany $Atendimentos
 */
class AtendidosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('atendidos');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Sexos', [
            'foreignKey' => 'sexo_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Sexos'
        ]);
        $this->belongsTo('Racas', [
            'foreignKey' => 'raca_id',
            'className' => 'Admin.Racas'
        ]);
        $this->belongsTo('Deficiencias', [
            'foreignKey' => 'deficiencia_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Deficiencias'
        ]);
        $this->hasMany('Atendimentos', [
            'foreignKey' => 'atendido_id',
            'className' => 'Admin.Atendimentos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nome');

        $validator
            ->add('nascimento', 'valid', ['rule' => 'date'])
            ->requirePresence('nascimento', 'create')
            ->notEmpty('nascimento');

        $validator
            ->requirePresence('cpf', 'create')
            ->notEmpty('cpf');

        $validator
            ->requirePresence('rg', 'create')
            ->notEmpty('rg');

        $validator
            ->requirePresence('pai', 'create')
            ->notEmpty('pai');

        $validator
            ->requirePresence('mae', 'create')
            ->notEmpty('mae');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sexo_id'], 'Sexos'));
        $rules->add($rules->existsIn(['raca_id'], 'Racas'));
        $rules->add($rules->existsIn(['deficiencia_id'], 'Deficiencias'));
        return $rules;
    }
}
