<?php
namespace Admin\Model\Table;

use Admin\Model\Entity\Localizaco;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Localizacoes Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Areas
 */
class LocalizacoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('localizacoes');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsToMany('Areas', [
            'foreignKey' => 'localizacao_id',
            'targetForeignKey' => 'area_id',
            'joinTable' => 'areas_localizacoes',
            'className' => 'Admin.Areas'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nome');

        return $validator;
    }
}
