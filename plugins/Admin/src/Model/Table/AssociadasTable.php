<?php
namespace Admin\Model\Table;

use Admin\Model\Entity\Associada;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Associadas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Municipios
 * @property \Cake\ORM\Association\HasMany $Atendimentos
 * @property \Cake\ORM\Association\BelongsToMany $Areas
 * @property \Cake\ORM\Association\BelongsToMany $Profissionais
 */
class AssociadasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('associadas');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Municipios', [
            'foreignKey' => 'municipio_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Municipios'
        ]);
        $this->hasMany('Atendimentos', [
            'foreignKey' => 'associada_id',
            'className' => 'Admin.Atendimentos'
        ]);
        $this->belongsToMany('Areas', [
            'foreignKey' => 'associada_id',
            'targetForeignKey' => 'area_id',
            'joinTable' => 'areas_associadas',
            'className' => 'Admin.Areas'
        ]);
        $this->belongsToMany('Profissionais', [
            'foreignKey' => 'associada_id',
            'targetForeignKey' => 'profissional_id',
            'joinTable' => 'associadas_profissionais',
            'className' => 'Admin.Profissionais'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['municipio_id'], 'Municipios'));
        return $rules;
    }
}
