<?php
namespace Admin\Model\Table;

use Admin\Model\Entity\Endereco;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Enderecos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Localizacoes
 */
class EnderecosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('enderecos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Localizacoes', [
            'foreignKey' => 'localizacao_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Localizacoes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('lougradouro');

        $validator
            ->allowEmpty('bairro');

        $validator
            ->allowEmpty('cep');

        $validator
            ->add('latitude', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('latitude');

        $validator
            ->add('longitude', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('longitude');

        $validator
            ->add('numero', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('numero');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['localizacao_id'], 'Localizacoes'));
        return $rules;
    }
}
