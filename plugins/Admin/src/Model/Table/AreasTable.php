<?php
namespace Admin\Model\Table;

use Admin\Model\Entity\Area;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Areas Model
 *
 * @property \Cake\ORM\Association\HasMany $Atendimentos
 * @property \Cake\ORM\Association\HasMany $Especialidades
 * @property \Cake\ORM\Association\HasMany $Profissionais
 * @property \Cake\ORM\Association\BelongsToMany $Associadas
 * @property \Cake\ORM\Association\BelongsToMany $Localizacoes
 * @property \Cake\ORM\Association\BelongsToMany $Profissionais
 */
class AreasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('areas');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('Atendimentos', [
            'foreignKey' => 'area_id',
            'className' => 'Admin.Atendimentos'
        ]);
        $this->hasMany('Especialidades', [
            'foreignKey' => 'area_id',
            'className' => 'Admin.Especialidades'
        ]);
        $this->hasMany('Profissionais', [
            'foreignKey' => 'area_id',
            'className' => 'Admin.Profissionais'
        ]);
        $this->belongsToMany('Associadas', [
            'foreignKey' => 'area_id',
            'targetForeignKey' => 'associada_id',
            'joinTable' => 'areas_associadas',
            'className' => 'Admin.Associadas'
        ]);
        $this->belongsToMany('Localizacoes', [
            'foreignKey' => 'area_id',
            'targetForeignKey' => 'localizaco_id',
            'joinTable' => 'areas_localizacoes',
            'className' => 'Admin.Localizacoes'
        ]);
        $this->belongsToMany('Profissionais', [
            'foreignKey' => 'area_id',
            'targetForeignKey' => 'profissionai_id',
            'joinTable' => 'areas_profissionais',
            'className' => 'Admin.Profissionais'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
