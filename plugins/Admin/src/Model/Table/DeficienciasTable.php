<?php
namespace Admin\Model\Table;

use Admin\Model\Entity\Deficiencia;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Deficiencias Model
 *
 * @property \Cake\ORM\Association\HasMany $Atendidos
 */
class DeficienciasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('deficiencias');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Atendidos', [
            'foreignKey' => 'deficiencia_id',
            'className' => 'Admin.Atendidos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
