<?php
namespace Admin\Model\Table;

use Admin\Model\Entity\Sexo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sexos Model
 *
 * @property \Cake\ORM\Association\HasMany $Atendidos
 * @property \Cake\ORM\Association\HasMany $Profissionais
 */
class SexosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sexos');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('Atendidos', [
            'foreignKey' => 'sexo_id',
            'className' => 'Admin.Atendidos'
        ]);
        $this->hasMany('Profissionais', [
            'foreignKey' => 'sexo_id',
            'className' => 'Admin.Profissionais'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
