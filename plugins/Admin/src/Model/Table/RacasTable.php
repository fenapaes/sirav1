<?php
namespace Admin\Model\Table;

use Admin\Model\Entity\Raca;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Racas Model
 *
 * @property \Cake\ORM\Association\HasMany $Atendidos
 */
class RacasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('racas');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('Atendidos', [
            'foreignKey' => 'raca_id',
            'className' => 'Admin.Atendidos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
