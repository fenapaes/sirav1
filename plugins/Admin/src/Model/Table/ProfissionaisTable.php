<?php
namespace Admin\Model\Table;

use Admin\Model\Entity\Profissionai;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Profissionais Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Sexos
 * @property \Cake\ORM\Association\BelongsTo $Areas
 * @property \Cake\ORM\Association\BelongsToMany $Areas
 * @property \Cake\ORM\Association\BelongsToMany $Associadas
 */
class ProfissionaisTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('profissionais');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Sexos', [
            'foreignKey' => 'sexo_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Sexos'
        ]);
        $this->belongsTo('Areas', [
            'foreignKey' => 'area_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Areas'
        ]);
        $this->belongsToMany('Areas', [
            'foreignKey' => 'profissional_id',
            'targetForeignKey' => 'area_id',
            'joinTable' => 'areas_profissionais',
            'className' => 'Admin.Areas'
        ]);
        $this->belongsToMany('Associadas', [
            'foreignKey' => 'profissional_id',
            'targetForeignKey' => 'associada_id',
            'joinTable' => 'associadas_profissionais',
            'className' => 'Admin.Associadas'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nome');

        $validator
            ->requirePresence('cpf', 'create')
            ->notEmpty('cpf');

        $validator
            ->add('rg', 'valid', ['rule' => 'numeric'])
            ->requirePresence('rg', 'create')
            ->notEmpty('rg');

        $validator
            ->add('nascimento', 'valid', ['rule' => 'date'])
            ->requirePresence('nascimento', 'create')
            ->notEmpty('nascimento');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sexo_id'], 'Sexos'));
        $rules->add($rules->existsIn(['area_id'], 'Areas'));
        return $rules;
    }
}
