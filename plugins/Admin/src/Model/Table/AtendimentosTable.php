<?php
namespace Admin\Model\Table;

use Admin\Model\Entity\Atendimento;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Atendimentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Associadas
 * @property \Cake\ORM\Association\BelongsTo $Atendidos
 * @property \Cake\ORM\Association\BelongsTo $Profissionais
 * @property \Cake\ORM\Association\BelongsTo $Areas
 * @property \Cake\ORM\Association\BelongsTo $Especialidades
 * @property \Cake\ORM\Association\BelongsTo $Localizacoes
 */
class AtendimentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('atendimentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Associadas', [
            'foreignKey' => 'associada_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Associadas'
        ]);
        $this->belongsTo('Atendidos', [
            'foreignKey' => 'atendido_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Atendidos'
        ]);
        $this->belongsTo('Profissionais', [
            'foreignKey' => 'profissional_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Profissionais'
        ]);
        $this->belongsTo('Areas', [
            'foreignKey' => 'area_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Areas'
        ]);
        $this->belongsTo('Especialidades', [
            'foreignKey' => 'especialidade_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Especialidades'
        ]);
        $this->belongsTo('Localizacoes', [
            'foreignKey' => 'localizacao_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Localizacoes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->add('data_inicio', 'valid', ['rule' => 'datetime'])
            ->requirePresence('data_inicio', 'create')
            ->notEmpty('data_inicio');

        $validator
            ->requirePresence('observacoes', 'create')
            ->notEmpty('observacoes');

        $validator
            ->add('data_fim', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('data_fim');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['associada_id'], 'Associadas'));
        $rules->add($rules->existsIn(['atendido_id'], 'Atendidos'));
        $rules->add($rules->existsIn(['profissional_id'], 'Profissionais'));
        $rules->add($rules->existsIn(['area_id'], 'Areas'));
        $rules->add($rules->existsIn(['especialidade_id'], 'Especialidades'));
        $rules->add($rules->existsIn(['localizacao_id'], 'Localizacoes'));
        return $rules;
    }
}
