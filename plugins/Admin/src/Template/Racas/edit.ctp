<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $raca->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $raca->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Racas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Atendidos'), ['controller' => 'Atendidos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Atendido'), ['controller' => 'Atendidos', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="racas form large-10 medium-9 columns">
    <?= $this->Form->create($raca) ?>
    <fieldset>
        <legend><?= __('Edit Raca') ?></legend>
        <?php
            echo $this->Form->input('nome');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
