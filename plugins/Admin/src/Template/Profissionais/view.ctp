<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Profissionai'), ['action' => 'edit', $profissionai->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Profissionai'), ['action' => 'delete', $profissionai->id], ['confirm' => __('Are you sure you want to delete # {0}?', $profissionai->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Profissionais'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Profissionai'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sexos'), ['controller' => 'Sexos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sexo'), ['controller' => 'Sexos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Areas'), ['controller' => 'Areas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Areas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Associadas'), ['controller' => 'Associadas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Associada'), ['controller' => 'Associadas', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="profissionais view large-10 medium-9 columns">
    <h2><?= h($profissionai->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Nome') ?></h6>
            <p><?= h($profissionai->nome) ?></p>
            <h6 class="subheader"><?= __('Sexo') ?></h6>
            <p><?= $profissionai->has('sexo') ? $this->Html->link($profissionai->sexo->id, ['controller' => 'Sexos', 'action' => 'view', $profissionai->sexo->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Cpf') ?></h6>
            <p><?= h($profissionai->cpf) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($profissionai->id) ?></p>
            <h6 class="subheader"><?= __('Area Id') ?></h6>
            <p><?= $this->Number->format($profissionai->area_id) ?></p>
            <h6 class="subheader"><?= __('Rg') ?></h6>
            <p><?= $this->Number->format($profissionai->rg) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Nascimento') ?></h6>
            <p><?= h($profissionai->nascimento) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Areas') ?></h4>
    <?php if (!empty($profissionai->areas)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Nome') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($profissionai->areas as $areas): ?>
        <tr>
            <td><?= h($areas->id) ?></td>
            <td><?= h($areas->nome) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Areas', 'action' => 'view', $areas->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Areas', 'action' => 'edit', $areas->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Areas', 'action' => 'delete', $areas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $areas->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Associadas') ?></h4>
    <?php if (!empty($profissionai->associadas)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Nome') ?></th>
            <th><?= __('Municipio Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($profissionai->associadas as $associadas): ?>
        <tr>
            <td><?= h($associadas->id) ?></td>
            <td><?= h($associadas->nome) ?></td>
            <td><?= h($associadas->municipio_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Associadas', 'action' => 'view', $associadas->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Associadas', 'action' => 'edit', $associadas->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Associadas', 'action' => 'delete', $associadas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $associadas->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
