<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $profissionai->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $profissionai->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Profissionais'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sexos'), ['controller' => 'Sexos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sexo'), ['controller' => 'Sexos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Areas'), ['controller' => 'Areas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Areas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Associadas'), ['controller' => 'Associadas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Associada'), ['controller' => 'Associadas', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="profissionais form large-10 medium-9 columns">
    <?= $this->Form->create($profissionai) ?>
    <fieldset>
        <legend><?= __('Edit Profissionai') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('sexo_id', ['options' => $sexos]);
            echo $this->Form->input('area_id');
            echo $this->Form->input('cpf');
            echo $this->Form->input('rg');
            echo $this->Form->input('nascimento');
            echo $this->Form->input('areas._ids', ['options' => $areas]);
            echo $this->Form->input('associadas._ids', ['options' => $associadas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
