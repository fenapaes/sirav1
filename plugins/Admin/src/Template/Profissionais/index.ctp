<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Profissionai'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sexos'), ['controller' => 'Sexos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sexo'), ['controller' => 'Sexos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Areas'), ['controller' => 'Areas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Areas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Associadas'), ['controller' => 'Associadas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Associada'), ['controller' => 'Associadas', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="profissionais index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th><?= $this->Paginator->sort('sexo_id') ?></th>
            <th><?= $this->Paginator->sort('area_id') ?></th>
            <th><?= $this->Paginator->sort('cpf') ?></th>
            <th><?= $this->Paginator->sort('rg') ?></th>
            <th><?= $this->Paginator->sort('nascimento') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($profissionais as $profissionai): ?>
        <tr>
            <td><?= $this->Number->format($profissionai->id) ?></td>
            <td><?= h($profissionai->nome) ?></td>
            <td>
                <?= $profissionai->has('sexo') ? $this->Html->link($profissionai->sexo->id, ['controller' => 'Sexos', 'action' => 'view', $profissionai->sexo->id]) : '' ?>
            </td>
            <td><?= $this->Number->format($profissionai->area_id) ?></td>
            <td><?= h($profissionai->cpf) ?></td>
            <td><?= $this->Number->format($profissionai->rg) ?></td>
            <td><?= h($profissionai->nascimento) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $profissionai->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $profissionai->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $profissionai->id], ['confirm' => __('Are you sure you want to delete # {0}?', $profissionai->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
