<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Municipio'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Associadas'), ['controller' => 'Associadas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Associada'), ['controller' => 'Associadas', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="municipios index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th><?= $this->Paginator->sort('estado_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($municipios as $municipio): ?>
        <tr>
            <td><?= $this->Number->format($municipio->id) ?></td>
            <td><?= h($municipio->nome) ?></td>
            <td>
                <?= $municipio->has('estado') ? $this->Html->link($municipio->estado->id, ['controller' => 'Estados', 'action' => 'view', $municipio->estado->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $municipio->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $municipio->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $municipio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipio->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
