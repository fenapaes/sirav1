<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Municipio'), ['action' => 'edit', $municipio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Municipio'), ['action' => 'delete', $municipio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Associadas'), ['controller' => 'Associadas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Associada'), ['controller' => 'Associadas', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="municipios view large-10 medium-9 columns">
    <h2><?= h($municipio->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Nome') ?></h6>
            <p><?= h($municipio->nome) ?></p>
            <h6 class="subheader"><?= __('Estado') ?></h6>
            <p><?= $municipio->has('estado') ? $this->Html->link($municipio->estado->id, ['controller' => 'Estados', 'action' => 'view', $municipio->estado->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($municipio->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Associadas') ?></h4>
    <?php if (!empty($municipio->associadas)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Nome') ?></th>
            <th><?= __('Municipio Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($municipio->associadas as $associadas): ?>
        <tr>
            <td><?= h($associadas->id) ?></td>
            <td><?= h($associadas->nome) ?></td>
            <td><?= h($associadas->municipio_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Associadas', 'action' => 'view', $associadas->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Associadas', 'action' => 'edit', $associadas->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Associadas', 'action' => 'delete', $associadas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $associadas->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
