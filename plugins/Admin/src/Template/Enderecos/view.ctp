<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Endereco'), ['action' => 'edit', $endereco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Endereco'), ['action' => 'delete', $endereco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $endereco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Enderecos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Endereco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Localizacoes'), ['controller' => 'Localizacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Localizaco'), ['controller' => 'Localizacoes', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="enderecos view large-10 medium-9 columns">
    <h2><?= h($endereco->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Lougradouro') ?></h6>
            <p><?= h($endereco->lougradouro) ?></p>
            <h6 class="subheader"><?= __('Localizaco') ?></h6>
            <p><?= $endereco->has('localizaco') ? $this->Html->link($endereco->localizaco->id, ['controller' => 'Localizacoes', 'action' => 'view', $endereco->localizaco->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Bairro') ?></h6>
            <p><?= h($endereco->bairro) ?></p>
            <h6 class="subheader"><?= __('Cep') ?></h6>
            <p><?= h($endereco->cep) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($endereco->id) ?></p>
            <h6 class="subheader"><?= __('Latitude') ?></h6>
            <p><?= $this->Number->format($endereco->latitude) ?></p>
            <h6 class="subheader"><?= __('Longitude') ?></h6>
            <p><?= $this->Number->format($endereco->longitude) ?></p>
            <h6 class="subheader"><?= __('Numero') ?></h6>
            <p><?= $this->Number->format($endereco->numero) ?></p>
        </div>
    </div>
</div>
