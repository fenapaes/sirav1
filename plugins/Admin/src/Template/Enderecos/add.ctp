<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Enderecos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Localizacoes'), ['controller' => 'Localizacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Localizaco'), ['controller' => 'Localizacoes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="enderecos form large-10 medium-9 columns">
    <?= $this->Form->create($endereco) ?>
    <fieldset>
        <legend><?= __('Add Endereco') ?></legend>
        <?php
            echo $this->Form->input('lougradouro');
            echo $this->Form->input('localizacao_id', ['options' => $localizacoes]);
            echo $this->Form->input('bairro');
            echo $this->Form->input('cep');
            echo $this->Form->input('latitude');
            echo $this->Form->input('longitude');
            echo $this->Form->input('numero');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
