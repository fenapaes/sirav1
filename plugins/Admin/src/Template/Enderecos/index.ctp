<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Endereco'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Localizacoes'), ['controller' => 'Localizacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Localizaco'), ['controller' => 'Localizacoes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="enderecos index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('lougradouro') ?></th>
            <th><?= $this->Paginator->sort('localizacao_id') ?></th>
            <th><?= $this->Paginator->sort('bairro') ?></th>
            <th><?= $this->Paginator->sort('cep') ?></th>
            <th><?= $this->Paginator->sort('latitude') ?></th>
            <th><?= $this->Paginator->sort('longitude') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($enderecos as $endereco): ?>
        <tr>
            <td><?= $this->Number->format($endereco->id) ?></td>
            <td><?= h($endereco->lougradouro) ?></td>
            <td>
                <?= $endereco->has('localizaco') ? $this->Html->link($endereco->localizaco->id, ['controller' => 'Localizacoes', 'action' => 'view', $endereco->localizaco->id]) : '' ?>
            </td>
            <td><?= h($endereco->bairro) ?></td>
            <td><?= h($endereco->cep) ?></td>
            <td><?= $this->Number->format($endereco->latitude) ?></td>
            <td><?= $this->Number->format($endereco->longitude) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $endereco->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $endereco->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $endereco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $endereco->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
