<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Localizacoes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Areas'), ['controller' => 'Areas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Areas', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="localizacoes form large-10 medium-9 columns">
    <?= $this->Form->create($localizaco) ?>
    <fieldset>
        <legend><?= __('Add Localizaco') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('areas._ids', ['options' => $areas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
