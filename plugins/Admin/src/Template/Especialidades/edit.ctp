<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $especialidade->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $especialidade->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Especialidades'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Areas'), ['controller' => 'Areas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Areas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Atendimentos'), ['controller' => 'Atendimentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Atendimento'), ['controller' => 'Atendimentos', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="especialidades form large-10 medium-9 columns">
    <?= $this->Form->create($especialidade) ?>
    <fieldset>
        <legend><?= __('Edit Especialidade') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('area_id', ['options' => $areas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
