<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Especialidade'), ['action' => 'edit', $especialidade->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Especialidade'), ['action' => 'delete', $especialidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $especialidade->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Especialidades'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Especialidade'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Areas'), ['controller' => 'Areas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Areas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Atendimentos'), ['controller' => 'Atendimentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Atendimento'), ['controller' => 'Atendimentos', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="especialidades view large-10 medium-9 columns">
    <h2><?= h($especialidade->nome) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Nome') ?></h6>
            <p><?= h($especialidade->nome) ?></p>
            <h6 class="subheader"><?= __('Area') ?></h6>
            <p><?= $especialidade->has('area') ? $this->Html->link($especialidade->area->nome, ['controller' => 'Areas', 'action' => 'view', $especialidade->area->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($especialidade->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Atendimentos') ?></h4>
    <?php if (!empty($especialidade->atendimentos)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Descricao') ?></th>
            <th><?= __('Associada Id') ?></th>
            <th><?= __('Atendido Id') ?></th>
            <th><?= __('Profissional Id') ?></th>
            <th><?= __('Area Id') ?></th>
            <th><?= __('Especialidade Id') ?></th>
            <th><?= __('Data Inicio') ?></th>
            <th><?= __('Observacoes') ?></th>
            <th><?= __('Localizacao Id') ?></th>
            <th><?= __('Data Fim') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($especialidade->atendimentos as $atendimentos): ?>
        <tr>
            <td><?= h($atendimentos->id) ?></td>
            <td><?= h($atendimentos->descricao) ?></td>
            <td><?= h($atendimentos->associada_id) ?></td>
            <td><?= h($atendimentos->atendido_id) ?></td>
            <td><?= h($atendimentos->profissional_id) ?></td>
            <td><?= h($atendimentos->area_id) ?></td>
            <td><?= h($atendimentos->especialidade_id) ?></td>
            <td><?= h($atendimentos->data_inicio) ?></td>
            <td><?= h($atendimentos->observacoes) ?></td>
            <td><?= h($atendimentos->localizacao_id) ?></td>
            <td><?= h($atendimentos->data_fim) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Atendimentos', 'action' => 'view', $atendimentos->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Atendimentos', 'action' => 'edit', $atendimentos->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Atendimentos', 'action' => 'delete', $atendimentos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $atendimentos->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
