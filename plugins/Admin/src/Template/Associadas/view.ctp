<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Associada'), ['action' => 'edit', $associada->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Associada'), ['action' => 'delete', $associada->id], ['confirm' => __('Are you sure you want to delete # {0}?', $associada->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Associadas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Associada'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Atendimentos'), ['controller' => 'Atendimentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Atendimento'), ['controller' => 'Atendimentos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Areas'), ['controller' => 'Areas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Areas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Profissionais'), ['controller' => 'Profissionais', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Profissionai'), ['controller' => 'Profissionais', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="associadas view large-10 medium-9 columns">
    <h2><?= h($associada->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Nome') ?></h6>
            <p><?= h($associada->nome) ?></p>
            <h6 class="subheader"><?= __('Municipio') ?></h6>
            <p><?= $associada->has('municipio') ? $this->Html->link($associada->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $associada->municipio->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($associada->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Atendimentos') ?></h4>
    <?php if (!empty($associada->atendimentos)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Descricao') ?></th>
            <th><?= __('Associada Id') ?></th>
            <th><?= __('Atendido Id') ?></th>
            <th><?= __('Profissional Id') ?></th>
            <th><?= __('Area Id') ?></th>
            <th><?= __('Especialidade Id') ?></th>
            <th><?= __('Data Inicio') ?></th>
            <th><?= __('Observacoes') ?></th>
            <th><?= __('Localizacao Id') ?></th>
            <th><?= __('Data Fim') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($associada->atendimentos as $atendimentos): ?>
        <tr>
            <td><?= h($atendimentos->id) ?></td>
            <td><?= h($atendimentos->descricao) ?></td>
            <td><?= h($atendimentos->associada_id) ?></td>
            <td><?= h($atendimentos->atendido_id) ?></td>
            <td><?= h($atendimentos->profissional_id) ?></td>
            <td><?= h($atendimentos->area_id) ?></td>
            <td><?= h($atendimentos->especialidade_id) ?></td>
            <td><?= h($atendimentos->data_inicio) ?></td>
            <td><?= h($atendimentos->observacoes) ?></td>
            <td><?= h($atendimentos->localizacao_id) ?></td>
            <td><?= h($atendimentos->data_fim) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Atendimentos', 'action' => 'view', $atendimentos->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Atendimentos', 'action' => 'edit', $atendimentos->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Atendimentos', 'action' => 'delete', $atendimentos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $atendimentos->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Areas') ?></h4>
    <?php if (!empty($associada->areas)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Nome') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($associada->areas as $areas): ?>
        <tr>
            <td><?= h($areas->id) ?></td>
            <td><?= h($areas->nome) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Areas', 'action' => 'view', $areas->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Areas', 'action' => 'edit', $areas->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Areas', 'action' => 'delete', $areas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $areas->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Profissionais') ?></h4>
    <?php if (!empty($associada->profissionais)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Nome') ?></th>
            <th><?= __('Sexo Id') ?></th>
            <th><?= __('Area Id') ?></th>
            <th><?= __('Cpf') ?></th>
            <th><?= __('Rg') ?></th>
            <th><?= __('Nascimento') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($associada->profissionais as $profissionais): ?>
        <tr>
            <td><?= h($profissionais->id) ?></td>
            <td><?= h($profissionais->nome) ?></td>
            <td><?= h($profissionais->sexo_id) ?></td>
            <td><?= h($profissionais->area_id) ?></td>
            <td><?= h($profissionais->cpf) ?></td>
            <td><?= h($profissionais->rg) ?></td>
            <td><?= h($profissionais->nascimento) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Profissionais', 'action' => 'view', $profissionais->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Profissionais', 'action' => 'edit', $profissionais->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Profissionais', 'action' => 'delete', $profissionais->id], ['confirm' => __('Are you sure you want to delete # {0}?', $profissionais->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
