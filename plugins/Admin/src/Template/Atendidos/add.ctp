<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Atendidos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sexos'), ['controller' => 'Sexos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sexo'), ['controller' => 'Sexos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Racas'), ['controller' => 'Racas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Raca'), ['controller' => 'Racas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Deficiencias'), ['controller' => 'Deficiencias', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Deficiencia'), ['controller' => 'Deficiencias', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Atendimentos'), ['controller' => 'Atendimentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Atendimento'), ['controller' => 'Atendimentos', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="atendidos form large-10 medium-9 columns">
    <?= $this->Form->create($atendido) ?>
    <fieldset>
        <legend><?= __('Add Atendido') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('nascimento');
            echo $this->Form->input('cpf');
            echo $this->Form->input('rg');
            echo $this->Form->input('pai');
            echo $this->Form->input('mae');
            echo $this->Form->input('sexo_id', ['options' => $sexos]);
            echo $this->Form->input('raca_id', ['options' => $racas, 'empty' => true]);
            echo $this->Form->input('deficiencia_id', ['options' => $deficiencias]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
