<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Atendido'), ['action' => 'edit', $atendido->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Atendido'), ['action' => 'delete', $atendido->id], ['confirm' => __('Are you sure you want to delete # {0}?', $atendido->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Atendidos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Atendido'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sexos'), ['controller' => 'Sexos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sexo'), ['controller' => 'Sexos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Racas'), ['controller' => 'Racas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Raca'), ['controller' => 'Racas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Deficiencias'), ['controller' => 'Deficiencias', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Deficiencia'), ['controller' => 'Deficiencias', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Atendimentos'), ['controller' => 'Atendimentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Atendimento'), ['controller' => 'Atendimentos', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="atendidos view large-10 medium-9 columns">
    <h2><?= h($atendido->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Nome') ?></h6>
            <p><?= h($atendido->nome) ?></p>
            <h6 class="subheader"><?= __('Cpf') ?></h6>
            <p><?= h($atendido->cpf) ?></p>
            <h6 class="subheader"><?= __('Rg') ?></h6>
            <p><?= h($atendido->rg) ?></p>
            <h6 class="subheader"><?= __('Pai') ?></h6>
            <p><?= h($atendido->pai) ?></p>
            <h6 class="subheader"><?= __('Mae') ?></h6>
            <p><?= h($atendido->mae) ?></p>
            <h6 class="subheader"><?= __('Sexo') ?></h6>
            <p><?= $atendido->has('sexo') ? $this->Html->link($atendido->sexo->id, ['controller' => 'Sexos', 'action' => 'view', $atendido->sexo->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Raca') ?></h6>
            <p><?= $atendido->has('raca') ? $this->Html->link($atendido->raca->id, ['controller' => 'Racas', 'action' => 'view', $atendido->raca->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Deficiencia') ?></h6>
            <p><?= $atendido->has('deficiencia') ? $this->Html->link($atendido->deficiencia->id, ['controller' => 'Deficiencias', 'action' => 'view', $atendido->deficiencia->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($atendido->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Nascimento') ?></h6>
            <p><?= h($atendido->nascimento) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Atendimentos') ?></h4>
    <?php if (!empty($atendido->atendimentos)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Descricao') ?></th>
            <th><?= __('Associada Id') ?></th>
            <th><?= __('Atendido Id') ?></th>
            <th><?= __('Profissional Id') ?></th>
            <th><?= __('Area Id') ?></th>
            <th><?= __('Especialidade Id') ?></th>
            <th><?= __('Data Inicio') ?></th>
            <th><?= __('Observacoes') ?></th>
            <th><?= __('Localizacao Id') ?></th>
            <th><?= __('Data Fim') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($atendido->atendimentos as $atendimentos): ?>
        <tr>
            <td><?= h($atendimentos->id) ?></td>
            <td><?= h($atendimentos->descricao) ?></td>
            <td><?= h($atendimentos->associada_id) ?></td>
            <td><?= h($atendimentos->atendido_id) ?></td>
            <td><?= h($atendimentos->profissional_id) ?></td>
            <td><?= h($atendimentos->area_id) ?></td>
            <td><?= h($atendimentos->especialidade_id) ?></td>
            <td><?= h($atendimentos->data_inicio) ?></td>
            <td><?= h($atendimentos->observacoes) ?></td>
            <td><?= h($atendimentos->localizacao_id) ?></td>
            <td><?= h($atendimentos->data_fim) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Atendimentos', 'action' => 'view', $atendimentos->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Atendimentos', 'action' => 'edit', $atendimentos->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Atendimentos', 'action' => 'delete', $atendimentos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $atendimentos->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
