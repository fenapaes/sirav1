<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Atendido'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sexos'), ['controller' => 'Sexos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sexo'), ['controller' => 'Sexos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Racas'), ['controller' => 'Racas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Raca'), ['controller' => 'Racas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Deficiencias'), ['controller' => 'Deficiencias', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Deficiencia'), ['controller' => 'Deficiencias', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Atendimentos'), ['controller' => 'Atendimentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Atendimento'), ['controller' => 'Atendimentos', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="atendidos index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th><?= $this->Paginator->sort('nascimento') ?></th>
            <th><?= $this->Paginator->sort('cpf') ?></th>
            <th><?= $this->Paginator->sort('rg') ?></th>
            <th><?= $this->Paginator->sort('pai') ?></th>
            <th><?= $this->Paginator->sort('mae') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($atendidos as $atendido): ?>
        <tr>
            <td><?= $this->Number->format($atendido->id) ?></td>
            <td><?= h($atendido->nome) ?></td>
            <td><?= h($atendido->nascimento) ?></td>
            <td><?= h($atendido->cpf) ?></td>
            <td><?= h($atendido->rg) ?></td>
            <td><?= h($atendido->pai) ?></td>
            <td><?= h($atendido->mae) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $atendido->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $atendido->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $atendido->id], ['confirm' => __('Are you sure you want to delete # {0}?', $atendido->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
