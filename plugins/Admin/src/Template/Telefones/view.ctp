<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Telefone'), ['action' => 'edit', $telefone->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Telefone'), ['action' => 'delete', $telefone->id], ['confirm' => __('Are you sure you want to delete # {0}?', $telefone->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Telefones'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Telefone'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Localizacoes'), ['controller' => 'Localizacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Localizaco'), ['controller' => 'Localizacoes', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="telefones view large-10 medium-9 columns">
    <h2><?= h($telefone->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Ddd') ?></h6>
            <p><?= h($telefone->ddd) ?></p>
            <h6 class="subheader"><?= __('Telefone') ?></h6>
            <p><?= h($telefone->telefone) ?></p>
            <h6 class="subheader"><?= __('Localizaco') ?></h6>
            <p><?= $telefone->has('localizaco') ? $this->Html->link($telefone->localizaco->id, ['controller' => 'Localizacoes', 'action' => 'view', $telefone->localizaco->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($telefone->id) ?></p>
        </div>
    </div>
</div>
