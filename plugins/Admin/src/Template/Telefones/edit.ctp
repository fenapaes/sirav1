<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $telefone->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $telefone->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Telefones'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Localizacoes'), ['controller' => 'Localizacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Localizaco'), ['controller' => 'Localizacoes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="telefones form large-10 medium-9 columns">
    <?= $this->Form->create($telefone) ?>
    <fieldset>
        <legend><?= __('Edit Telefone') ?></legend>
        <?php
            echo $this->Form->input('ddd');
            echo $this->Form->input('telefone');
            echo $this->Form->input('localizacao_id', ['options' => $localizacoes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
