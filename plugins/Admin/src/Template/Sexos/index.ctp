<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Sexo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Atendidos'), ['controller' => 'Atendidos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Atendido'), ['controller' => 'Atendidos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Profissionais'), ['controller' => 'Profissionais', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Profissionai'), ['controller' => 'Profissionais', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="sexos index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($sexos as $sexo): ?>
        <tr>
            <td><?= $this->Number->format($sexo->id) ?></td>
            <td><?= h($sexo->nome) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $sexo->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sexo->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sexo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sexo->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
