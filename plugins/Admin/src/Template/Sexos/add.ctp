<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Sexos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Atendidos'), ['controller' => 'Atendidos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Atendido'), ['controller' => 'Atendidos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Profissionais'), ['controller' => 'Profissionais', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Profissionai'), ['controller' => 'Profissionais', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="sexos form large-10 medium-9 columns">
    <?= $this->Form->create($sexo) ?>
    <fieldset>
        <legend><?= __('Add Sexo') ?></legend>
        <?php
            echo $this->Form->input('nome');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
