<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Area'), ['action' => 'edit', $area->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Area'), ['action' => 'delete', $area->id], ['confirm' => __('Are you sure you want to delete # {0}?', $area->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Areas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Area'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Atendimentos'), ['controller' => 'Atendimentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Atendimento'), ['controller' => 'Atendimentos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Especialidades'), ['controller' => 'Especialidades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Especialidade'), ['controller' => 'Especialidades', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Profissionais'), ['controller' => 'Profissionais', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Profissionai'), ['controller' => 'Profissionais', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Associadas'), ['controller' => 'Associadas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Associada'), ['controller' => 'Associadas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Localizacoes'), ['controller' => 'Localizacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Localizaco'), ['controller' => 'Localizacoes', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="areas view large-10 medium-9 columns">
    <h2><?= h($area->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Nome') ?></h6>
            <p><?= h($area->nome) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($area->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Atendimentos') ?></h4>
    <?php if (!empty($area->atendimentos)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Descricao') ?></th>
            <th><?= __('Associada Id') ?></th>
            <th><?= __('Atendido Id') ?></th>
            <th><?= __('Profissional Id') ?></th>
            <th><?= __('Area Id') ?></th>
            <th><?= __('Especialidade Id') ?></th>
            <th><?= __('Data Inicio') ?></th>
            <th><?= __('Observacoes') ?></th>
            <th><?= __('Localizacao Id') ?></th>
            <th><?= __('Data Fim') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($area->atendimentos as $atendimentos): ?>
        <tr>
            <td><?= h($atendimentos->id) ?></td>
            <td><?= h($atendimentos->descricao) ?></td>
            <td><?= h($atendimentos->associada_id) ?></td>
            <td><?= h($atendimentos->atendido_id) ?></td>
            <td><?= h($atendimentos->profissional_id) ?></td>
            <td><?= h($atendimentos->area_id) ?></td>
            <td><?= h($atendimentos->especialidade_id) ?></td>
            <td><?= h($atendimentos->data_inicio) ?></td>
            <td><?= h($atendimentos->observacoes) ?></td>
            <td><?= h($atendimentos->localizacao_id) ?></td>
            <td><?= h($atendimentos->data_fim) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Atendimentos', 'action' => 'view', $atendimentos->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Atendimentos', 'action' => 'edit', $atendimentos->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Atendimentos', 'action' => 'delete', $atendimentos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $atendimentos->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Especialidades') ?></h4>
    <?php if (!empty($area->especialidades)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Nome') ?></th>
            <th><?= __('Area Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($area->especialidades as $especialidades): ?>
        <tr>
            <td><?= h($especialidades->id) ?></td>
            <td><?= h($especialidades->nome) ?></td>
            <td><?= h($especialidades->area_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Especialidades', 'action' => 'view', $especialidades->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Especialidades', 'action' => 'edit', $especialidades->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Especialidades', 'action' => 'delete', $especialidades->id], ['confirm' => __('Are you sure you want to delete # {0}?', $especialidades->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Profissionais') ?></h4>
    <?php if (!empty($area->profissionais)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Nome') ?></th>
            <th><?= __('Sexo Id') ?></th>
            <th><?= __('Area Id') ?></th>
            <th><?= __('Cpf') ?></th>
            <th><?= __('Rg') ?></th>
            <th><?= __('Nascimento') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($area->profissionais as $profissionais): ?>
        <tr>
            <td><?= h($profissionais->id) ?></td>
            <td><?= h($profissionais->nome) ?></td>
            <td><?= h($profissionais->sexo_id) ?></td>
            <td><?= h($profissionais->area_id) ?></td>
            <td><?= h($profissionais->cpf) ?></td>
            <td><?= h($profissionais->rg) ?></td>
            <td><?= h($profissionais->nascimento) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Profissionais', 'action' => 'view', $profissionais->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Profissionais', 'action' => 'edit', $profissionais->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Profissionais', 'action' => 'delete', $profissionais->id], ['confirm' => __('Are you sure you want to delete # {0}?', $profissionais->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Associadas') ?></h4>
    <?php if (!empty($area->associadas)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Nome') ?></th>
            <th><?= __('Municipio Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($area->associadas as $associadas): ?>
        <tr>
            <td><?= h($associadas->id) ?></td>
            <td><?= h($associadas->nome) ?></td>
            <td><?= h($associadas->municipio_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Associadas', 'action' => 'view', $associadas->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Associadas', 'action' => 'edit', $associadas->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Associadas', 'action' => 'delete', $associadas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $associadas->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Localizacoes') ?></h4>
    <?php if (!empty($area->localizacoes)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Nome') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($area->localizacoes as $localizacoes): ?>
        <tr>
            <td><?= h($localizacoes->id) ?></td>
            <td><?= h($localizacoes->nome) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Localizacoes', 'action' => 'view', $localizacoes->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Localizacoes', 'action' => 'edit', $localizacoes->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Localizacoes', 'action' => 'delete', $localizacoes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $localizacoes->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
