<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Deficiencia'), ['action' => 'edit', $deficiencia->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Deficiencia'), ['action' => 'delete', $deficiencia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $deficiencia->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Deficiencias'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Deficiencia'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Atendidos'), ['controller' => 'Atendidos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Atendido'), ['controller' => 'Atendidos', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="deficiencias view large-10 medium-9 columns">
    <h2><?= h($deficiencia->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Nome') ?></h6>
            <p><?= h($deficiencia->nome) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($deficiencia->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Atendidos') ?></h4>
    <?php if (!empty($deficiencia->atendidos)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Nome') ?></th>
            <th><?= __('Nascimento') ?></th>
            <th><?= __('Cpf') ?></th>
            <th><?= __('Rg') ?></th>
            <th><?= __('Pai') ?></th>
            <th><?= __('Mae') ?></th>
            <th><?= __('Sexo Id') ?></th>
            <th><?= __('Raca Id') ?></th>
            <th><?= __('Deficiencia Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($deficiencia->atendidos as $atendidos): ?>
        <tr>
            <td><?= h($atendidos->id) ?></td>
            <td><?= h($atendidos->nome) ?></td>
            <td><?= h($atendidos->nascimento) ?></td>
            <td><?= h($atendidos->cpf) ?></td>
            <td><?= h($atendidos->rg) ?></td>
            <td><?= h($atendidos->pai) ?></td>
            <td><?= h($atendidos->mae) ?></td>
            <td><?= h($atendidos->sexo_id) ?></td>
            <td><?= h($atendidos->raca_id) ?></td>
            <td><?= h($atendidos->deficiencia_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Atendidos', 'action' => 'view', $atendidos->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Atendidos', 'action' => 'edit', $atendidos->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Atendidos', 'action' => 'delete', $atendidos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $atendidos->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
