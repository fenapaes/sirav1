<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Atendimento'), ['action' => 'edit', $atendimento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Atendimento'), ['action' => 'delete', $atendimento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $atendimento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Atendimentos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Atendimento'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Associadas'), ['controller' => 'Associadas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Associada'), ['controller' => 'Associadas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Atendidos'), ['controller' => 'Atendidos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Atendido'), ['controller' => 'Atendidos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Profissionais'), ['controller' => 'Profissionais', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Profissionai'), ['controller' => 'Profissionais', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Areas'), ['controller' => 'Areas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Areas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Especialidades'), ['controller' => 'Especialidades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Especialidade'), ['controller' => 'Especialidades', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Localizacoes'), ['controller' => 'Localizacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Localizaco'), ['controller' => 'Localizacoes', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="atendimentos view large-10 medium-9 columns">
    <h2><?= h($atendimento->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Associada') ?></h6>
            <p><?= $atendimento->has('associada') ? $this->Html->link($atendimento->associada->id, ['controller' => 'Associadas', 'action' => 'view', $atendimento->associada->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Atendido') ?></h6>
            <p><?= $atendimento->has('atendido') ? $this->Html->link($atendimento->atendido->id, ['controller' => 'Atendidos', 'action' => 'view', $atendimento->atendido->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Profissionai') ?></h6>
            <p><?= $atendimento->has('profissionai') ? $this->Html->link($atendimento->profissionai->id, ['controller' => 'Profissionais', 'action' => 'view', $atendimento->profissionai->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Area') ?></h6>
            <p><?= $atendimento->has('area') ? $this->Html->link($atendimento->area->id, ['controller' => 'Areas', 'action' => 'view', $atendimento->area->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Especialidade') ?></h6>
            <p><?= $atendimento->has('especialidade') ? $this->Html->link($atendimento->especialidade->id, ['controller' => 'Especialidades', 'action' => 'view', $atendimento->especialidade->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Localizaco') ?></h6>
            <p><?= $atendimento->has('localizaco') ? $this->Html->link($atendimento->localizaco->id, ['controller' => 'Localizacoes', 'action' => 'view', $atendimento->localizaco->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($atendimento->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Data Inicio') ?></h6>
            <p><?= h($atendimento->data_inicio) ?></p>
            <h6 class="subheader"><?= __('Data Fim') ?></h6>
            <p><?= h($atendimento->data_fim) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Descricao') ?></h6>
            <?= $this->Text->autoParagraph(h($atendimento->descricao)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Observacoes') ?></h6>
            <?= $this->Text->autoParagraph(h($atendimento->observacoes)) ?>
        </div>
    </div>
</div>
