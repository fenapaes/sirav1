<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Atendimento'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Associadas'), ['controller' => 'Associadas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Associada'), ['controller' => 'Associadas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Atendidos'), ['controller' => 'Atendidos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Atendido'), ['controller' => 'Atendidos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Profissionais'), ['controller' => 'Profissionais', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Profissionai'), ['controller' => 'Profissionais', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Areas'), ['controller' => 'Areas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Areas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Especialidades'), ['controller' => 'Especialidades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Especialidade'), ['controller' => 'Especialidades', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Localizacoes'), ['controller' => 'Localizacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Localizaco'), ['controller' => 'Localizacoes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="atendimentos index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('associada_id') ?></th>
            <th><?= $this->Paginator->sort('atendido_id') ?></th>
            <th><?= $this->Paginator->sort('profissional_id') ?></th>
            <th><?= $this->Paginator->sort('area_id') ?></th>
            <th><?= $this->Paginator->sort('especialidade_id') ?></th>
            <th><?= $this->Paginator->sort('data_inicio') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($atendimentos as $atendimento): ?>
        <tr>
            <td><?= $this->Number->format($atendimento->id) ?></td>
            <td>
                <?= $atendimento->has('associada') ? $this->Html->link($atendimento->associada->id, ['controller' => 'Associadas', 'action' => 'view', $atendimento->associada->id]) : '' ?>
            </td>
            <td>
                <?= $atendimento->has('atendido') ? $this->Html->link($atendimento->atendido->id, ['controller' => 'Atendidos', 'action' => 'view', $atendimento->atendido->id]) : '' ?>
            </td>
            <td>
                <?= $atendimento->has('profissionai') ? $this->Html->link($atendimento->profissionai->id, ['controller' => 'Profissionais', 'action' => 'view', $atendimento->profissionai->id]) : '' ?>
            </td>
            <td>
                <?= $atendimento->has('area') ? $this->Html->link($atendimento->area->id, ['controller' => 'Areas', 'action' => 'view', $atendimento->area->id]) : '' ?>
            </td>
            <td>
                <?= $atendimento->has('especialidade') ? $this->Html->link($atendimento->especialidade->id, ['controller' => 'Especialidades', 'action' => 'view', $atendimento->especialidade->id]) : '' ?>
            </td>
            <td><?= h($atendimento->data_inicio) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $atendimento->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $atendimento->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $atendimento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $atendimento->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
