<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $atendimento->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $atendimento->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Atendimentos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Associadas'), ['controller' => 'Associadas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Associada'), ['controller' => 'Associadas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Atendidos'), ['controller' => 'Atendidos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Atendido'), ['controller' => 'Atendidos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Profissionais'), ['controller' => 'Profissionais', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Profissionai'), ['controller' => 'Profissionais', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Areas'), ['controller' => 'Areas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Areas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Especialidades'), ['controller' => 'Especialidades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Especialidade'), ['controller' => 'Especialidades', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Localizacoes'), ['controller' => 'Localizacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Localizaco'), ['controller' => 'Localizacoes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="atendimentos form large-10 medium-9 columns">
    <?= $this->Form->create($atendimento) ?>
    <fieldset>
        <legend><?= __('Edit Atendimento') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('associada_id', ['options' => $associadas]);
            echo $this->Form->input('atendido_id', ['options' => $atendidos]);
            echo $this->Form->input('profissional_id', ['options' => $profissionais]);
            echo $this->Form->input('area_id', ['options' => $areas]);
            echo $this->Form->input('especialidade_id', ['options' => $especialidades]);
            echo $this->Form->input('data_inicio');
            echo $this->Form->input('observacoes');
            echo $this->Form->input('localizacao_id', ['options' => $localizacoes]);
            echo $this->Form->input('data_fim');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
