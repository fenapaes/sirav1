<?php
namespace Admin\Controller;

use Admin\Controller\AppController;

/**
 * Especialidades Controller
 *
 * @property \Admin\Model\Table\EspecialidadesTable $Especialidades
 */
class EspecialidadesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Areas']
        ];
        $this->set('especialidades', $this->paginate($this->Especialidades));
        $this->set('_serialize', ['especialidades']);
    }

    /**
     * View method
     *
     * @param string|null $id Especialidade id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $especialidade = $this->Especialidades->get($id, [
            'contain' => ['Areas', 'Atendimentos']
        ]);
        $this->set('especialidade', $especialidade);
        $this->set('_serialize', ['especialidade']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $especialidade = $this->Especialidades->newEntity();
        if ($this->request->is('post')) {
            $especialidade = $this->Especialidades->patchEntity($especialidade, $this->request->data);
            if ($this->Especialidades->save($especialidade)) {
                $this->Flash->success(__('The especialidade has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The especialidade could not be saved. Please, try again.'));
            }
        }
        $areas = $this->Especialidades->Areas->find('list', ['limit' => 200]);
        $this->set(compact('especialidade', 'areas'));
        $this->set('_serialize', ['especialidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Especialidade id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $especialidade = $this->Especialidades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $especialidade = $this->Especialidades->patchEntity($especialidade, $this->request->data);
            if ($this->Especialidades->save($especialidade)) {
                $this->Flash->success(__('The especialidade has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The especialidade could not be saved. Please, try again.'));
            }
        }
        $areas = $this->Especialidades->Areas->find('list', ['limit' => 200]);
        $this->set(compact('especialidade', 'areas'));
        $this->set('_serialize', ['especialidade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Especialidade id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $especialidade = $this->Especialidades->get($id);
        if ($this->Especialidades->delete($especialidade)) {
            $this->Flash->success(__('The especialidade has been deleted.'));
        } else {
            $this->Flash->error(__('The especialidade could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
