<?php
namespace Admin\Controller;

use Admin\Controller\AppController;

/**
 * Racas Controller
 *
 * @property \Admin\Model\Table\RacasTable $Racas
 */
class RacasController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('racas', $this->paginate($this->Racas));
        $this->set('_serialize', ['racas']);
    }

    /**
     * View method
     *
     * @param string|null $id Raca id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $raca = $this->Racas->get($id, [
            'contain' => ['Atendidos']
        ]);
        $this->set('raca', $raca);
        $this->set('_serialize', ['raca']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $raca = $this->Racas->newEntity();
        if ($this->request->is('post')) {
            $raca = $this->Racas->patchEntity($raca, $this->request->data);
            if ($this->Racas->save($raca)) {
                $this->Flash->success(__('The raca has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The raca could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('raca'));
        $this->set('_serialize', ['raca']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Raca id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $raca = $this->Racas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $raca = $this->Racas->patchEntity($raca, $this->request->data);
            if ($this->Racas->save($raca)) {
                $this->Flash->success(__('The raca has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The raca could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('raca'));
        $this->set('_serialize', ['raca']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Raca id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $raca = $this->Racas->get($id);
        if ($this->Racas->delete($raca)) {
            $this->Flash->success(__('The raca has been deleted.'));
        } else {
            $this->Flash->error(__('The raca could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
