<?php
namespace Admin\Controller;

use Admin\Controller\AppController;

/**
 * Atendimentos Controller
 *
 * @property \Admin\Model\Table\AtendimentosTable $Atendimentos
 */
class AtendimentosController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Associadas', 'Atendidos', 'Profissionais', 'Areas', 'Especialidades', 'Localizacoes']
        ];
        $this->set('atendimentos', $this->paginate($this->Atendimentos));
        $this->set('_serialize', ['atendimentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Atendimento id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $atendimento = $this->Atendimentos->get($id, [
            'contain' => ['Associadas', 'Atendidos', 'Profissionais', 'Areas', 'Especialidades', 'Localizacoes']
        ]);
        $this->set('atendimento', $atendimento);
        $this->set('_serialize', ['atendimento']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $atendimento = $this->Atendimentos->newEntity();
        if ($this->request->is('post')) {
            $atendimento = $this->Atendimentos->patchEntity($atendimento, $this->request->data);
            if ($this->Atendimentos->save($atendimento)) {
                $this->Flash->success(__('The atendimento has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The atendimento could not be saved. Please, try again.'));
            }
        }
        $associadas = $this->Atendimentos->Associadas->find('list', ['limit' => 200]);
        $atendidos = $this->Atendimentos->Atendidos->find('list', ['limit' => 200]);
        $profissionais = $this->Atendimentos->Profissionais->find('list', ['limit' => 200]);
        $areas = $this->Atendimentos->Areas->find('list', ['limit' => 200]);
        $especialidades = $this->Atendimentos->Especialidades->find('list', ['limit' => 200]);
        $localizacoes = $this->Atendimentos->Localizacoes->find('list', ['limit' => 200]);
        $this->set(compact('atendimento', 'associadas', 'atendidos', 'profissionais', 'areas', 'especialidades', 'localizacoes'));
        $this->set('_serialize', ['atendimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atendimento id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $atendimento = $this->Atendimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimento = $this->Atendimentos->patchEntity($atendimento, $this->request->data);
            if ($this->Atendimentos->save($atendimento)) {
                $this->Flash->success(__('The atendimento has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The atendimento could not be saved. Please, try again.'));
            }
        }
        $associadas = $this->Atendimentos->Associadas->find('list', ['limit' => 200]);
        $atendidos = $this->Atendimentos->Atendidos->find('list', ['limit' => 200]);
        $profissionais = $this->Atendimentos->Profissionais->find('list', ['limit' => 200]);
        $areas = $this->Atendimentos->Areas->find('list', ['limit' => 200]);
        $especialidades = $this->Atendimentos->Especialidades->find('list', ['limit' => 200]);
        $localizacoes = $this->Atendimentos->Localizacoes->find('list', ['limit' => 200]);
        $this->set(compact('atendimento', 'associadas', 'atendidos', 'profissionais', 'areas', 'especialidades', 'localizacoes'));
        $this->set('_serialize', ['atendimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atendimento id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $atendimento = $this->Atendimentos->get($id);
        if ($this->Atendimentos->delete($atendimento)) {
            $this->Flash->success(__('The atendimento has been deleted.'));
        } else {
            $this->Flash->error(__('The atendimento could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
