<?php
namespace Admin\Controller;

use Admin\Controller\AppController;

/**
 * Atendidos Controller
 *
 * @property \Admin\Model\Table\AtendidosTable $Atendidos
 */
class AtendidosController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Sexos', 'Racas', 'Deficiencias']
        ];
        $this->set('atendidos', $this->paginate($this->Atendidos));
        $this->set('_serialize', ['atendidos']);
    }

    /**
     * View method
     *
     * @param string|null $id Atendido id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $atendido = $this->Atendidos->get($id, [
            'contain' => ['Sexos', 'Racas', 'Deficiencias', 'Atendimentos']
        ]);
        $this->set('atendido', $atendido);
        $this->set('_serialize', ['atendido']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $atendido = $this->Atendidos->newEntity();
        if ($this->request->is('post')) {
            $atendido = $this->Atendidos->patchEntity($atendido, $this->request->data);
            if ($this->Atendidos->save($atendido)) {
                $this->Flash->success(__('The atendido has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The atendido could not be saved. Please, try again.'));
            }
        }
        $sexos = $this->Atendidos->Sexos->find('list', ['limit' => 200]);
        $racas = $this->Atendidos->Racas->find('list', ['limit' => 200]);
        $deficiencias = $this->Atendidos->Deficiencias->find('list', ['limit' => 200]);
        $this->set(compact('atendido', 'sexos', 'racas', 'deficiencias'));
        $this->set('_serialize', ['atendido']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atendido id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $atendido = $this->Atendidos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendido = $this->Atendidos->patchEntity($atendido, $this->request->data);
            if ($this->Atendidos->save($atendido)) {
                $this->Flash->success(__('The atendido has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The atendido could not be saved. Please, try again.'));
            }
        }
        $sexos = $this->Atendidos->Sexos->find('list', ['limit' => 200]);
        $racas = $this->Atendidos->Racas->find('list', ['limit' => 200]);
        $deficiencias = $this->Atendidos->Deficiencias->find('list', ['limit' => 200]);
        $this->set(compact('atendido', 'sexos', 'racas', 'deficiencias'));
        $this->set('_serialize', ['atendido']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atendido id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $atendido = $this->Atendidos->get($id);
        if ($this->Atendidos->delete($atendido)) {
            $this->Flash->success(__('The atendido has been deleted.'));
        } else {
            $this->Flash->error(__('The atendido could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
