<?php
namespace Admin\Controller;

use Admin\Controller\AppController;

/**
 * Deficiencias Controller
 *
 * @property \Admin\Model\Table\DeficienciasTable $Deficiencias
 */
class DeficienciasController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('deficiencias', $this->paginate($this->Deficiencias));
        $this->set('_serialize', ['deficiencias']);
    }

    /**
     * View method
     *
     * @param string|null $id Deficiencia id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $deficiencia = $this->Deficiencias->get($id, [
            'contain' => ['Atendidos']
        ]);
        $this->set('deficiencia', $deficiencia);
        $this->set('_serialize', ['deficiencia']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deficiencia = $this->Deficiencias->newEntity();
        if ($this->request->is('post')) {
            $deficiencia = $this->Deficiencias->patchEntity($deficiencia, $this->request->data);
            if ($this->Deficiencias->save($deficiencia)) {
                $this->Flash->success(__('The deficiencia has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The deficiencia could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('deficiencia'));
        $this->set('_serialize', ['deficiencia']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Deficiencia id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $deficiencia = $this->Deficiencias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $deficiencia = $this->Deficiencias->patchEntity($deficiencia, $this->request->data);
            if ($this->Deficiencias->save($deficiencia)) {
                $this->Flash->success(__('The deficiencia has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The deficiencia could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('deficiencia'));
        $this->set('_serialize', ['deficiencia']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Deficiencia id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $deficiencia = $this->Deficiencias->get($id);
        if ($this->Deficiencias->delete($deficiencia)) {
            $this->Flash->success(__('The deficiencia has been deleted.'));
        } else {
            $this->Flash->error(__('The deficiencia could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
