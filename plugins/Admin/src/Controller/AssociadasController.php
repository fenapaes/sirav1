<?php
namespace Admin\Controller;

use Admin\Controller\AppController;

/**
 * Associadas Controller
 *
 * @property \Admin\Model\Table\AssociadasTable $Associadas
 */
class AssociadasController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Municipios']
        ];
        $this->set('associadas', $this->paginate($this->Associadas));
        $this->set('_serialize', ['associadas']);
    }

    /**
     * View method
     *
     * @param string|null $id Associada id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $associada = $this->Associadas->get($id, [
            'contain' => ['Municipios', 'Areas', 'Profissionais', 'Atendimentos']
        ]);
        $this->set('associada', $associada);
        $this->set('_serialize', ['associada']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $associada = $this->Associadas->newEntity();
        if ($this->request->is('post')) {
            $associada = $this->Associadas->patchEntity($associada, $this->request->data);
            if ($this->Associadas->save($associada)) {
                $this->Flash->success(__('The associada has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The associada could not be saved. Please, try again.'));
            }
        }
        $municipios = $this->Associadas->Municipios->find('list', ['limit' => 200]);
        $areas = $this->Associadas->Areas->find('list', ['limit' => 200]);
        $profissionais = $this->Associadas->Profissionais->find('list', ['limit' => 200]);
        $this->set(compact('associada', 'municipios', 'areas', 'profissionais'));
        $this->set('_serialize', ['associada']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Associada id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $associada = $this->Associadas->get($id, [
            'contain' => ['Areas', 'Profissionais']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $associada = $this->Associadas->patchEntity($associada, $this->request->data);
            if ($this->Associadas->save($associada)) {
                $this->Flash->success(__('The associada has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The associada could not be saved. Please, try again.'));
            }
        }
        $municipios = $this->Associadas->Municipios->find('list', ['limit' => 200]);
        $areas = $this->Associadas->Areas->find('list', ['limit' => 200]);
        $profissionais = $this->Associadas->Profissionais->find('list', ['limit' => 200]);
        $this->set(compact('associada', 'municipios', 'areas', 'profissionais'));
        $this->set('_serialize', ['associada']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Associada id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $associada = $this->Associadas->get($id);
        if ($this->Associadas->delete($associada)) {
            $this->Flash->success(__('The associada has been deleted.'));
        } else {
            $this->Flash->error(__('The associada could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
