<?php
namespace Admin\Controller;

use Admin\Controller\AppController;

/**
 * Localizacoes Controller
 *
 * @property \Admin\Model\Table\LocalizacoesTable $Localizacoes
 */
class LocalizacoesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('localizacoes', $this->paginate($this->Localizacoes));
        $this->set('_serialize', ['localizacoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Localizaco id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $localizaco = $this->Localizacoes->get($id, [
            'contain' => ['Areas']
        ]);
        $this->set('localizaco', $localizaco);
        $this->set('_serialize', ['localizaco']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $localizaco = $this->Localizacoes->newEntity();
        if ($this->request->is('post')) {
            $localizaco = $this->Localizacoes->patchEntity($localizaco, $this->request->data);
            if ($this->Localizacoes->save($localizaco)) {
                $this->Flash->success(__('The localizaco has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The localizaco could not be saved. Please, try again.'));
            }
        }
        $areas = $this->Localizacoes->Areas->find('list', ['limit' => 200]);
        $this->set(compact('localizaco', 'areas'));
        $this->set('_serialize', ['localizaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Localizaco id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $localizaco = $this->Localizacoes->get($id, [
            'contain' => ['Areas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $localizaco = $this->Localizacoes->patchEntity($localizaco, $this->request->data);
            if ($this->Localizacoes->save($localizaco)) {
                $this->Flash->success(__('The localizaco has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The localizaco could not be saved. Please, try again.'));
            }
        }
        $areas = $this->Localizacoes->Areas->find('list', ['limit' => 200]);
        $this->set(compact('localizaco', 'areas'));
        $this->set('_serialize', ['localizaco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Localizaco id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $localizaco = $this->Localizacoes->get($id);
        if ($this->Localizacoes->delete($localizaco)) {
            $this->Flash->success(__('The localizaco has been deleted.'));
        } else {
            $this->Flash->error(__('The localizaco could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
