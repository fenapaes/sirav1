<?php
namespace Admin\Controller;

use Admin\Controller\AppController;

/**
 * Profissionais Controller
 *
 * @property \Admin\Model\Table\ProfissionaisTable $Profissionais
 */
class ProfissionaisController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Sexos']
        ];
        $this->set('profissionais', $this->paginate($this->Profissionais));
        $this->set('_serialize', ['profissionais']);
    }

    /**
     * View method
     *
     * @param string|null $id Profissionai id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $profissionai = $this->Profissionais->get($id, [
            'contain' => ['Sexos', 'Areas', 'Associadas']
        ]);
        $this->set('profissionai', $profissionai);
        $this->set('_serialize', ['profissionai']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $profissionai = $this->Profissionais->newEntity();
        if ($this->request->is('post')) {
            $profissionai = $this->Profissionais->patchEntity($profissionai, $this->request->data);
            if ($this->Profissionais->save($profissionai)) {
                $this->Flash->success(__('The profissionai has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The profissionai could not be saved. Please, try again.'));
            }
        }
        $sexos = $this->Profissionais->Sexos->find('list', ['limit' => 200]);
        $areas = $this->Profissionais->Areas->find('list', ['limit' => 200]);
        $associadas = $this->Profissionais->Associadas->find('list', ['limit' => 200]);
        $this->set(compact('profissionai', 'sexos', 'areas', 'associadas'));
        $this->set('_serialize', ['profissionai']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Profissionai id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $profissionai = $this->Profissionais->get($id, [
            'contain' => ['Areas', 'Associadas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $profissionai = $this->Profissionais->patchEntity($profissionai, $this->request->data);
            if ($this->Profissionais->save($profissionai)) {
                $this->Flash->success(__('The profissionai has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The profissionai could not be saved. Please, try again.'));
            }
        }
        $sexos = $this->Profissionais->Sexos->find('list', ['limit' => 200]);
        $areas = $this->Profissionais->Areas->find('list', ['limit' => 200]);
        $associadas = $this->Profissionais->Associadas->find('list', ['limit' => 200]);
        $this->set(compact('profissionai', 'sexos', 'areas', 'associadas'));
        $this->set('_serialize', ['profissionai']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Profissionai id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $profissionai = $this->Profissionais->get($id);
        if ($this->Profissionais->delete($profissionai)) {
            $this->Flash->success(__('The profissionai has been deleted.'));
        } else {
            $this->Flash->error(__('The profissionai could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
