<?php
namespace Admin\Controller;

use Admin\Controller\AppController;

/**
 * Telefones Controller
 *
 * @property \Admin\Model\Table\TelefonesTable $Telefones
 */
class TelefonesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Localizacoes']
        ];
        $this->set('telefones', $this->paginate($this->Telefones));
        $this->set('_serialize', ['telefones']);
    }

    /**
     * View method
     *
     * @param string|null $id Telefone id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $telefone = $this->Telefones->get($id, [
            'contain' => ['Localizacoes']
        ]);
        $this->set('telefone', $telefone);
        $this->set('_serialize', ['telefone']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $telefone = $this->Telefones->newEntity();
        if ($this->request->is('post')) {
            $telefone = $this->Telefones->patchEntity($telefone, $this->request->data);
            if ($this->Telefones->save($telefone)) {
                $this->Flash->success(__('The telefone has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The telefone could not be saved. Please, try again.'));
            }
        }
        $localizacoes = $this->Telefones->Localizacoes->find('list', ['limit' => 200]);
        $this->set(compact('telefone', 'localizacoes'));
        $this->set('_serialize', ['telefone']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Telefone id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $telefone = $this->Telefones->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $telefone = $this->Telefones->patchEntity($telefone, $this->request->data);
            if ($this->Telefones->save($telefone)) {
                $this->Flash->success(__('The telefone has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The telefone could not be saved. Please, try again.'));
            }
        }
        $localizacoes = $this->Telefones->Localizacoes->find('list', ['limit' => 200]);
        $this->set(compact('telefone', 'localizacoes'));
        $this->set('_serialize', ['telefone']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Telefone id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $telefone = $this->Telefones->get($id);
        if ($this->Telefones->delete($telefone)) {
            $this->Flash->success(__('The telefone has been deleted.'));
        } else {
            $this->Flash->error(__('The telefone could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
