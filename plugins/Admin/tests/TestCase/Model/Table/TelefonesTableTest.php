<?php
namespace Admin\Test\TestCase\Model\Table;

use Admin\Model\Table\TelefonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Table\TelefonesTable Test Case
 */
class TelefonesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.telefones',
        'plugin.admin.localizacoes',
        'plugin.admin.areas',
        'plugin.admin.atendimentos',
        'plugin.admin.associadas',
        'plugin.admin.municipios',
        'plugin.admin.estados',
        'plugin.admin.areas_associadas',
        'plugin.admin.profissionais',
        'plugin.admin.sexos',
        'plugin.admin.atendidos',
        'plugin.admin.racas',
        'plugin.admin.deficiencias',
        'plugin.admin.areas_profissionais',
        'plugin.admin.associadas_profissionais',
        'plugin.admin.especialidades',
        'plugin.admin.areas_localizacoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Telefones') ? [] : ['className' => 'Admin\Model\Table\TelefonesTable'];
        $this->Telefones = TableRegistry::get('Telefones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Telefones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
