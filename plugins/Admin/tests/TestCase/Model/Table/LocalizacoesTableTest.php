<?php
namespace Admin\Test\TestCase\Model\Table;

use Admin\Model\Table\LocalizacoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Table\LocalizacoesTable Test Case
 */
class LocalizacoesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.localizacoes',
        'plugin.admin.areas',
        'plugin.admin.atendimentos',
        'plugin.admin.associadas',
        'plugin.admin.municipios',
        'plugin.admin.areas_associadas',
        'plugin.admin.profissionais',
        'plugin.admin.associadas_profissionais',
        'plugin.admin.atendidos',
        'plugin.admin.sexos',
        'plugin.admin.racas',
        'plugin.admin.deficiencias',
        'plugin.admin.especialidades',
        'plugin.admin.areas_profissionais',
        'plugin.admin.areas_localizacoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Localizacoes') ? [] : ['className' => 'Admin\Model\Table\LocalizacoesTable'];
        $this->Localizacoes = TableRegistry::get('Localizacoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Localizacoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
