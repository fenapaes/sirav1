<?php
namespace Admin\Test\TestCase\Model\Table;

use Admin\Model\Table\MunicipiosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Table\MunicipiosTable Test Case
 */
class MunicipiosTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.municipios',
        'plugin.admin.estados',
        'plugin.admin.associadas',
        'plugin.admin.atendimentos',
        'plugin.admin.atendidos',
        'plugin.admin.sexos',
        'plugin.admin.profissionais',
        'plugin.admin.racas',
        'plugin.admin.deficiencias',
        'plugin.admin.areas',
        'plugin.admin.especialidades',
        'plugin.admin.areas_profissionais',
        'plugin.admin.areas_associadas',
        'plugin.admin.localizacoes',
        'plugin.admin.areas_localizacoes',
        'plugin.admin.associadas_profissionais'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Municipios') ? [] : ['className' => 'Admin\Model\Table\MunicipiosTable'];
        $this->Municipios = TableRegistry::get('Municipios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Municipios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
