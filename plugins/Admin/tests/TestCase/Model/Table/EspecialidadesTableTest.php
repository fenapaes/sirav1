<?php
namespace Admin\Test\TestCase\Model\Table;

use Admin\Model\Table\EspecialidadesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Table\EspecialidadesTable Test Case
 */
class EspecialidadesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.especialidades',
        'plugin.admin.areas',
        'plugin.admin.atendimentos',
        'plugin.admin.associadas',
        'plugin.admin.municipios',
        'plugin.admin.areas_associadas',
        'plugin.admin.profissionais',
        'plugin.admin.associadas_profissionais',
        'plugin.admin.atendidos',
        'plugin.admin.sexos',
        'plugin.admin.racas',
        'plugin.admin.deficiencias',
        'plugin.admin.localizacoes',
        'plugin.admin.areas_profissionais',
        'plugin.admin.areas_localizacoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Especialidades') ? [] : ['className' => 'Admin\Model\Table\EspecialidadesTable'];
        $this->Especialidades = TableRegistry::get('Especialidades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Especialidades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
