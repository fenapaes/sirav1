<?php
namespace Admin\Test\TestCase\Model\Table;

use Admin\Model\Table\ProfissionaisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Table\ProfissionaisTable Test Case
 */
class ProfissionaisTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.profissionais',
        'plugin.admin.sexos',
        'plugin.admin.atendidos',
        'plugin.admin.racas',
        'plugin.admin.deficiencias',
        'plugin.admin.atendimentos',
        'plugin.admin.associadas',
        'plugin.admin.municipios',
        'plugin.admin.estados',
        'plugin.admin.areas',
        'plugin.admin.especialidades',
        'plugin.admin.areas_profissionais',
        'plugin.admin.areas_associadas',
        'plugin.admin.localizacoes',
        'plugin.admin.areas_localizacoes',
        'plugin.admin.associadas_profissionais'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Profissionais') ? [] : ['className' => 'Admin\Model\Table\ProfissionaisTable'];
        $this->Profissionais = TableRegistry::get('Profissionais', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Profissionais);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
