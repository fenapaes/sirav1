<?php
namespace Admin\Test\TestCase\Model\Table;

use Admin\Model\Table\AreasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Table\AreasTable Test Case
 */
class AreasTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.areas',
        'plugin.admin.atendimentos',
        'plugin.admin.associadas',
        'plugin.admin.atendidos',
        'plugin.admin.sexos',
        'plugin.admin.profissionais',
        'plugin.admin.racas',
        'plugin.admin.deficiencias',
        'plugin.admin.especialidades',
        'plugin.admin.localizacoes',
        'plugin.admin.areas_profissionais',
        'plugin.admin.areas_associadas',
        'plugin.admin.areas_localizacoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Areas') ? [] : ['className' => 'Admin\Model\Table\AreasTable'];
        $this->Areas = TableRegistry::get('Areas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Areas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
