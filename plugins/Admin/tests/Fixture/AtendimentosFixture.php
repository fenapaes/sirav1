<?php
namespace Admin\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AtendimentosFixture
 *
 */
class AtendimentosFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'descricao' => ['type' => 'text', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'associada_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'atendido_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'profissional_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'area_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'especialidade_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'data_inicio' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'observacoes' => ['type' => 'text', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'localizacao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'data_fim' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'profissional_id' => ['type' => 'index', 'columns' => ['profissional_id'], 'length' => []],
            'area_id' => ['type' => 'index', 'columns' => ['area_id'], 'length' => []],
            'especialidade_id' => ['type' => 'index', 'columns' => ['especialidade_id'], 'length' => []],
            'data' => ['type' => 'index', 'columns' => ['data_inicio'], 'length' => []],
            'atendido_id' => ['type' => 'index', 'columns' => ['atendido_id'], 'length' => []],
            'fk_atendimentos_associadas1_idx' => ['type' => 'index', 'columns' => ['associada_id'], 'length' => []],
            'fk_atendimentos_localizacoes1_idx' => ['type' => 'index', 'columns' => ['localizacao_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'atendimentos_ibfk_1' => ['type' => 'foreign', 'columns' => ['profissional_id'], 'references' => ['profissionais', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'atendimentos_ibfk_2' => ['type' => 'foreign', 'columns' => ['area_id'], 'references' => ['areas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'atendimentos_ibfk_3' => ['type' => 'foreign', 'columns' => ['especialidade_id'], 'references' => ['especialidades', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'atendimentos_ibfk_4' => ['type' => 'foreign', 'columns' => ['atendido_id'], 'references' => ['atendidos', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_atendimentos_associadas1' => ['type' => 'foreign', 'columns' => ['associada_id'], 'references' => ['associadas', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_atendimentos_localizacoes1' => ['type' => 'foreign', 'columns' => ['localizacao_id'], 'references' => ['localizacoes', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'descricao' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'associada_id' => 1,
            'atendido_id' => 1,
            'profissional_id' => 1,
            'area_id' => 1,
            'especialidade_id' => 1,
            'data_inicio' => '2015-08-19 03:33:09',
            'observacoes' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'localizacao_id' => 1,
            'data_fim' => '2015-08-19 03:33:09'
        ],
    ];
}
